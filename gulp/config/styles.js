/**
 * Override the Default
 * Core Styles
 * Config
 *
 */
module.exports = function (cb) {
    var filterCSS = filter('**/*.css', { restore: true });

    return gulp.src(config.paths.src)
        .pipe(plumber(function (error) {
            console.log(error.message);
            this.emit('end');}))
        .pipe(sass({
            includePaths: require('bourbon').includePaths,
            includePaths: require('node-neat').includePaths
        }))
        .pipe(sourcemaps.init())
        .pipe(sass.sync(config.options.sass))
        .pipe(autoprefixer(config.options.autoprefixer))
        .pipe(sourcemaps.write('./'))
        // .pipe(scsslint({'config': 'scss-lint.yml'}))
        .pipe(gulp.dest(config.paths.dest))

        .pipe(filterCSS)
        .pipe(browserSync.reload({ stream: true }))
        .pipe(filterCSS.restore)

        .pipe(notify({
            message: 'SCSS Compiled.',
            onLast: true
        }));
};