/**
 * Override the Default
 * Core Scripts
 * Config
 *
 */
var path = require('path');
var webpack = require('webpack-stream').webpack;
var BowerWebpackPlugin = require('bower-webpack-plugin');

module.exports = {
    options: {
        webpack: {

            // merged with defaults
            // for :watch task
            watch: {
                cache: true,
                watch: true,
                devtool: 'eval',
                keepalive: true,
            },

            // merged with defaults
            // for :dev task
            dev: {
                devtool: 'eval'
            },


            // merged with defaults
            // for :prod task
            prod: {
                plugins: [
                    new webpack.optimize.DedupePlugin(),
                    new webpack.optimize.OccurenceOrderPlugin(true),
                    new webpack.optimize.UglifyJsPlugin({
                        sourceMap: false,
                        comments: false,
                        screw_ie8: true,
                        compress: {
                            drop_console: true,
                            pure_getters: true,
                            unsafe: true,
                            unsafe_comps: true,
                            screw_ie8: true,
                            warnings: false
                        }
                    })
                ],
                eslint: {
                    failOnError: false, // THIS IS TEMP !!!!!
                    failOnWarning: false
                }
            },

            defaults: {
                entry: {
                    main: './assets/js/pages/main'
                },
                resolve: {
                    extensions: ['', '.js', '.jsx']
                },
                output: {
                    path: path.join(__dirname, 'js'),
                    filename: '[name].js',
                    chunkFilename: 'chunk-[name].js'
                },
                stats: {
                    colors: true
                },
                module: {
                    preLoaders: [
                        {
                            test: /\.jsx?$/,
                            exclude: [
                                /node_modules/,
                                /bower_components/,
                                /vendor/,
                                /polyfills/
                            ],
                            loader: 'eslint'
                        }
                    ],
                    loaders: [
                        {
                            test: /\.jsx?$/,
                            exclude: [
                                /node_modules/,
                                /bower_components/,
                                /polyfills/
                            ],
                            loader: 'babel',
                            query: {
                                presets: ['es2015']
                            }
                        }
                    ]
                },
                plugins: [
                    new BowerWebpackPlugin({
                        includes: /\.jsx?$/
                    }),
                    new webpack.ProvidePlugin({
                        '$': 'jquery',
                        'jQuery': 'jquery',
                        'window.jQuery': 'jquery'
                    }),
                ],
                eslint: {
                    emitError: true,
                    emitWarning: true,
                    configFile: path.resolve('./.eslintrc')
                }
            }

        }
    }
};

