var gulp         = require('gulp');
var plumber      = require('gulp-plumber');
var sass         = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var minify       = require('gulp-cssnano');
var notify       = require('gulp-notify');

// config
var config       = require('../../config/styles');


/**
 * Compile SCSS to CSS
 * and Minify
 *
 */
module.exports = function () {
	return gulp.src(config.paths.src)
		.pipe(plumber(function (error) {
			console.log(error.message);
			this.emit('end');}))
		.pipe(sass({
			includePaths: require('bourbon').includePaths,
			includePaths: require('node-neat').includePaths
		}))

		.pipe(minify(config.options.minify))

		.pipe(gulp.dest(config.paths.dest))
		.pipe(notify({
			message: 'SCSS Compiled & Minified.',
			onLast: true
		}));
};