import Base from '../modules/base';
import _ from 'underscore';
export default new (Base.extend({

    bindings: {},

    bindedObjects: {},

    initialize() {
        this.bind();
    },

    register(bindings) {
        _.extend(this.bindings, bindings);
        return this;
    },

    bind() {
        _.each(this.bindings, (Module, selector) => {
            let id = selector.replace('.js-', '');

            this.bindedObjects[id] = this.bindedObjects[id] || [];
            $(selector).each((index, element) => {
                let $element = $(element);

                if (!$element.data('initialized')) {
                    let objectIndex = this.bindedObjects[id].length;
                    $element.data('binding-id', id + objectIndex);
                    let object = new Module($element);
                    this.bindedObjects[id].push(object);
                    $element.data('initialized', true);
                }
            });
        });
    }

}))();

