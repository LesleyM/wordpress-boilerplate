let options =  {

    defaultOptions: {
        width: 300,
        height: 600,
        text: 'age',
        color: 'red',
        textColor: 'white',
        value: 'population',
        onClick: function(d) {
            area_two.svg.selectAll('text').transition()
            .duration(1000).style('fill', 'red');
            area_two.svg.selectAll('body').transition().duration(2000).style('fill', 'blue');
        }
    },

    testOptions: {
         idth: 400,
        height: 400,
        text: 'age',
        color: 'red',
        textColor: 'white',
        value: 'population',
        onClick: function(d) {
            area_two.svg.selectAll('text').transition()
            .duration(1000).style('fill', 'red');
            area_two.svg.selectAll('body').transition().duration(2000).style('fill', 'blue');
        }
    }
};

export default options;
