let options =  {

    
    var map_options = {
            center: [52.16003714829255, 5.264418009652125],
            zoom: 18.5, 
            scrollWheelZoom: false,
            doubleClickZoom: false
        },
        bounds = [
            [50.780940, 3.177019],
            [53.559242, 7.511126]
        ];

    var popup;
    var pallette = [
            '#1a9850',
            '#66bd63',
            '#a6d96a',
            '#d9ef8b',
            '#ffffbf',
            '#fee08b',
            '#fdae61',
            '#f46d43',
            '#d73027'
        ],
        hoverBorderColor = '#13417e';

    var labels = [
            {
                property: 'alle_misdrijven',
                title: 'Alle misdrijven',
                description: 'Alle misdrijven'
            }
            // {
            //     property: 'inbraak_woning_totaal',
            //     title: 'Diefstal/inbraak woning',
            //     description: 'Diefstal/inbraak woning'
            // },
            // {
            //     property: 'inbraak_garage',
            //     title: ' Diefstal/inbraak box/garage/schuur/tuinhuis',
            //     description: ' Diefstal/inbraak box/garage/schuur/tuinhuis'
            // }
            // {
            //     property: 'diefstal_uit_vanaf_motorvoertuigen',
            //     title: 'Diefstal uit/vanaf motorvoertuigen',
            //     description: 'Diefstal uit/vanaf motorvoertuigen'
            // },
            // {
            //     property: 'diefstal_motorvoertuigen',
            //     title: 'Diefstal van motorvoertuigen',
            //     description: 'Diefstal van motorvoertuigen'
            // },

            // {
            //     property: 'inbraak_bedrijven',
            //     title: ' Diefstal/inbraak bedrijven en instellingen',
            //     description: 'Diefstal/inbraak bedrijven en instellingen'
            // },
            // {
            //     property: 'drugshandel',
            //     title: ' Drugshandel',
            //     description: 'Drugshandel'
            // },
            // {
            //     property: 'zedenmisdrijf',
            //     title: ' Zedenmisdrijf',
            //     description: 'Zedenmisdrijf'
            // }


        ],
        property2Label = {};

    /**
     * OBJECTS
     */
    var map = L.map('map', map_options);

    var view,
        currentVar = labels[0].property;

    var data,
        minMax, //   {min: 0, max: 0, thresholds:[]},
        feature2Gemeente ,  // Maps feature code to data object, e.g. provinces[20] = {  }
        gemeentes = {
            names: [],
            abbreviations: [],
            featureLayers: {}
        },
        currentGemeente,
        currentGemeenteIndex;

    
}

export default options;