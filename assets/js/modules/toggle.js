import Base from './base';

export default Base.extend({

    selectors: {
        trigger: '.js-trigger',
        target: '.js-target',
        indicator: '.js-indicator',
        class: '.js-class',
    },

    attributes: {
        opend: 'data-toggle-class',
        state: 'data-toggle-state',
        openText: 'data-open',
        closeText: 'data-closed'
    },

    state: null,

    initialize($element)  {
        this._super('initialize', $element);
        this.$element = $element;
        this.dataCloseText = $(this.selectors.indicator, this.$element).attr(this.attributes.closeText);
        this.dataOpenText = $(this.selectors.indicator, this.$element).attr(this.attributes.openText);
        this.dataClass = $(this.selectors.class, this.$element).attr(this.attributes.opend);
        this.setState();
    },

    initBindings() {
        $(this.$element).on('click', this.selectors.trigger, $.proxy(this.toggleTabs, this));
    },

    toggleTabs() {
        $(this.selectors.target, this.$element).toggleClass(this.dataClass);
        this.setState();
    },

    setState() {
        this.state = $(this.selectors.indicator, this.$element).attr(this.attributes.state) == 'closed' ? 'open' : 'closed';
        $(this.selectors.indicator).attr(this.attributes.state, this.state);
        //TODO do not check for attr but only check in code
        this.checkState();
    },

    checkState() {
        this.state == 'open' ? $(this.selectors.indicator, this.$element).text(this.attributes.dataOpenText) : $(this.selectors.indicator, this.$element).text(this.dataCloseText);
    }
});
