import _ from 'underscore';

var Base = function () {
    this.initialize.apply(this, arguments);
    this.initBindings.apply(this, arguments);
};

_.extend(Base.prototype, {

    initialize($element) {},

    initBindings() {},

    _findParentObject(methodName, child) {
        var object = child;
        while (object[methodName] === child[methodName]) {
            object = object.constructor.__super__;
        }
        return object;
    },

    _super(methodName) {

        this._currentObjects || (this._currentObjects = {});

        var currentObject = this._currentObjects[methodName] || this,
            parentObject = this._findParentObject(methodName, currentObject);

        this._currentObjects[methodName] = parentObject;
        if (parentObject[methodName]) {
            var result = parentObject[methodName].apply(this, Array.prototype.slice.call(arguments, 1));
            delete this._currentObjects[methodName];
            return result;
        }
    },

    _collect(property) {
        var values = collect.call(this, property),
            isArray = _.isArray(_.first(values));

        return _.reduce(values, function (result, value) {
            if (isArray) {
                return _.union(result, value);
            } else {
                return $.extend(true, {}, result, value);
            }
        }, isArray ? [] : {});
    },

    dispose() {}
});

var collect = function (property) {
    var objects = (this[property]) ? [this[property]] : [],
        currentConstructor = this.constructor,
        currentProto = currentConstructor.prototype;

    do {
        if (currentProto.hasOwnProperty(property)) {  // skip over any prototype that doesn't define 'events' itself
            objects.unshift(currentProto[property]);  // prepend so that when we run _.extend() to do the merge, subclasses take precedence
        }
    } while (currentConstructor = (currentProto = currentConstructor.__super__) && currentProto.constructor);

    return objects;
};

// Copied from BACKBONE source
// Helper function to correctly set up the prototype chain, for subclasses.
// Similar to `goog.inherits`, but uses a hash of prototype properties and
// class properties to be extended.
Base.extend = function (protoProps, staticProps) {
    var parent = this;
    var child;

    // The constructor function for the new subclass is either defined by you
    // (the "constructor" property in your `extend` definition), or defaulted
    // by us to simply call the parent's constructor.
    if (protoProps && _.has(protoProps, 'constructor')) {
        child = protoProps.constructor;
    } else {
        child = function () {
            return parent.apply(this, arguments);
        };
    }

    // Add static properties to the constructor function, if supplied.
    _.extend(child, parent, staticProps);

    // Set the prototype chain to inherit from `parent`, without calling
    // `parent`'s constructor function.
    var Surrogate = function () {
        this.constructor = child;
    };
    Surrogate.prototype = parent.prototype;
    child.prototype = new Surrogate;

    // Add prototype properties (instance properties) to the subclass,
    // if supplied.
    if (protoProps) _.extend(child.prototype, protoProps);

    // Set a convenience property in case the parent's prototype is needed
    // later.
    child.__super__ = parent.prototype;

    return child;
};

export default Base;