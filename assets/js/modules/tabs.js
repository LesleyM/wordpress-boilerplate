import Base from './base';

export default Base.extend({

    selectors: {
        trigger: '.js-tab-item',
        target: '.js-tab-content',
        tab: 'tab'
    },

    classes: {
        active: 'active'
    },

    initialize($element)  {
        this._super('initialize', $element);
        this.$element = $element;
    },

    initBindings() {
        $(this.$element).on('click', this.selectors.trigger, $.proxy(this.toggleTabs, this));
    },

    toggleTabs(evt) {
        const tab_id = $(evt.currentTarget).data('tab');
        $(this.selectors.trigger, this.$element).removeClass('active');
        $(this.selectors.target, this.$element).removeClass('show');
        $(evt.currentTarget, this.$element).addClass('active');
        $(`#${tab_id}`, this.$element).addClass('show');
    }
});
