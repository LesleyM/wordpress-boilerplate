import Base from '../modules/base';
import binder from '../helpers/binder';

export default Base.extend({

    initialize() {
        binder.register(this._collect('bindings')).bind();
    }

});