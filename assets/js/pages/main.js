import Base from '../pages/base';
import Tabs from '../modules/tabs';
import Toggle from '../modules/toggle';

export default new (Base.extend({

    bindings: {
        '.js-tabs': Tabs,
        '.js-toggle': Toggle
    },

    initialize($element) {
        this._super('initialize', $element);
    }
}));
